﻿using System;
using OddNumbersLib;

namespace ManualTests
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var oddLib = new OddNumbers();

            var odds = oddLib.GetOddNumbersFromRange(1, 99);

            foreach (var odd in odds)
            {
                Console.WriteLine(odd);
            }

            Console.ReadLine();
        }
    }
}