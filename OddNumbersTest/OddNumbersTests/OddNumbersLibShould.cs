﻿using System;
using NUnit.Framework;
using OddNumbersLib;

namespace OddNumbersTests
{
    [TestFixture]
    public class OddNumbersLibShould
    {
        private OddNumbers CreateOddNumber()
        {
            return new OddNumbers();
        }

        [Test]
        public void ReturnsOddNumbers_IfInput_IsRange()
        {
            int[] expectedResult = {3, 5, 7, 9};
            var oddLib = CreateOddNumber();


            var result = oddLib.GetOddNumbersFromRange(2, 10);

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void ReturnsTrue_IfNumber_IsOdd()
        {
            var oddLib = CreateOddNumber();

            var odd = oddLib.IsOdd(5);

            Assert.IsTrue(odd);
        }

        [Test]
        public void ThowrsArgumentException_IfStartAndEndOfRange_AreEqual()
        {
            var oddLib = CreateOddNumber();

            Assert.Throws<ArgumentException>(() => oddLib.GetOddNumbersFromRange(2, 2));
        }
    }
}