﻿using System;
using System.Collections.Generic;

namespace OddNumbersLib
{
    public class OddNumbers
    {
        public bool IsOdd(int number)
        {
            if (number%2 != 0)
            {
                return true;
            }

            return false;
        }

        public int[] GetOddNumbersFromRange(int start, int end)
        {
            var odds = new List<int>();

            if (start <= end)
            {
                if (!IsOdd(start))
                {
                    start++;
                }

                for (var i = start; i < end; i += 2)
                {
                    odds.Add(i);
                }

                if (IsOdd(end))
                {
                    odds.Add(end);
                }
            }
            else
            {
                throw new ArgumentException(string.Format("Границы {0} и {1} не являются диапазоном", start, end));
            }


            return odds.ToArray();
        }
    }
}